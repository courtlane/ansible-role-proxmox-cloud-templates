proxmox-cloud-templates
=========

Create and update cloud image VM templates in Proxmox VE.

The provided cloud images will be downloaded and customized using the `libguestfstools` package to ensure the `qemu-guest-agent` is installed within the templates.

Requirements
------------

**Required collections:**
  - community.general

**Python modules (Will be installed automatically in a Python virtualenv):**
  - proxmoxer 
  - requests

Role Variables
--------------
The default values for the variables are set in `defaults/main.yml`:
```yml
# Cloud images to be downloaded and created as VM templates on Proxmox
images:
  - name: debian-12-cloudinit-template
    url: https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-genericcloud-amd64.qcow2
    vmid: 9000
  - name: debian-11-cloudinit-template
    url: https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2
    vmid: 9001
  - name: ubuntu-24.04-cloudinit-template
    url: https://cloud-images.ubuntu.com/releases/noble/release/ubuntu-24.04-server-cloudimg-amd64.img
    vmid: 9002
  - name: ubuntu-22.04-cloudinit-template
    url: https://cloud-images.ubuntu.com/releases/jammy/release/ubuntu-22.04-server-cloudimg-amd64.img
    vmid: 9003

# Proxmox node variables
proxmox_api_host: '' # Requred: Proxmox API Host
proxmox_node: '' # Required: Node name should match the display name in the Proxmox UI

# Storage for VM templates
template_storage: local

# Download directory for images 
download_dir: /tmp/

# Proxmox API credentials - defaults to lookup Bash environment variables
# It is recommended to set credentials as environment variables
# Example: `export PM_USER='pve_user@pam'`
PM_USER: "{{ lookup('env','PM_USER') }}"
PM_PASS: "{{ lookup('env','PM_PASS') }}"

# VM Template default configuration (proxmox_vm module documentation for detailed info)
vm_boot: c
vm_bootdisk: virtio0
vm_net0: 'virtio,bridge=vmbr0'
vm_ide2: "{{ template_storage }}:cloudinit,format=qcow2"
vm_memory: 2048
vm_scsihw: virtio-scsi-pci
vm_serial0: socket
vm_vga: serial0
```

Example Inventory
-----------------
```yml
all:
  children:
    proxmox:
      hosts:
        pve:
          ansible_host: 192.168.178.10
```

Example Playbook
----------------
```yml
- hosts: proxmox
  become: yes
  roles:
    - role: proxmox-cloud-templates
      vars:
        proxmox_api_host: '192.168.178.10'
        proxmox_node: 'pve'
        template_storage: local
        # It is recommended to set credentials as environment variables
        # Example: `export PM_USER='pve_user@pam'`
        # Uncomment and set the following variables if you prefer to define them here
        # PM_USER: "root@pam"
        # PM_PASS: ""
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko 